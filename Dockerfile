FROM node:15

EXPOSE 8080

RUN mkdir -p /app
COPY . /app

WORKDIR /app

RUN npm install

CMD npm run serve